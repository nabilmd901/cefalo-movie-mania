import { Suspense } from "react";
import { useRoutes } from "react-router-dom";
import Loader from "./components/Loader";
import { ROUTES } from "./routes";

function App() {
  const routes = useRoutes(ROUTES);
  return <Suspense fallback={<Loader />}>{routes}</Suspense>;
}

export default App;
