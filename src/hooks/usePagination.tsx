import { Dispatch, SetStateAction, useEffect, useState } from "react";

export interface IPaginatedData<T> {
  currentPage: number;
  currentPageCount: number;
  limit: number;
  totalPage: number;
  data: T[];
}

export function usePagination<T>({
  data,
  limit = 10,
}: {
  data: T[];
  limit?: number;
}): [IPaginatedData<T>, Dispatch<SetStateAction<number>>] {
  const [currentPage, setCurrentPage] = useState(1);
  const [paginatedData, setPaginatedData] = useState<any>();
  const [currentPageCount, setCurrentPageCount] = useState(0);

  const paginate = (array: any, limit: number, pageNumber: number) => {
    const currentPageData = array.slice(
      (pageNumber - 1) * limit,
      pageNumber * limit
    );
    setCurrentPageCount(currentPageData.length);
    return currentPageData;
  };

  useEffect(() => {
    setPaginatedData([...paginate(data, limit, currentPage)]);
  }, [data, limit, currentPage]);

  return [
    {
      currentPage,
      currentPageCount,
      limit,
      totalPage: Math.ceil(data.length / limit),
      data: paginatedData,
    },
    setCurrentPage,
  ];
}
