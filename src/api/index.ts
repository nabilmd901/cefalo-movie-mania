import { BaseURL, ApiKey } from "../constants";

export const GET_GENRE_LIST = `${BaseURL}/genre/movie/list?api_key=${ApiKey}`;
export const GET_MOVIES_BY_GENRE = `${BaseURL}/discover/movie?api_key=${ApiKey}`;
export const GET_MOVIE_BY_ID = `${BaseURL}/movie`;