import axios, { AxiosResponse } from "axios";
import { useQuery } from "react-query";
import { GET_MOVIE_BY_ID } from "..";
import { ApiKey } from "../../constants";
import { IGetMovieCreditsByIdResponse } from "../contracts";

export function useGetCastAndCrew(id?: string) {
  const getCastAndCrew = async () => {
    let params: any = {
      api_key: ApiKey,
    };
    return (
      await axios.get<null, AxiosResponse<IGetMovieCreditsByIdResponse>>(
        `${GET_MOVIE_BY_ID}/${id}/credits`,
        { params }
      )
    ).data;
  };
  return useQuery(`get-movies-cast-crew-${id}`, getCastAndCrew);
}
