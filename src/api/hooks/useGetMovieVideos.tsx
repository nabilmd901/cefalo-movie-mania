import axios, { AxiosResponse } from "axios";
import { useQuery } from "react-query";
import { GET_MOVIE_BY_ID } from "..";
import { ApiKey } from "../../constants";
import { IGetMovieVideoResponse } from "../contracts";

export function useGetMovieVideos(id?: string) {
  const getMovieVideos = async () => {
    let params: any = {
      api_key: ApiKey,
    };
    return (
      await axios.get<null, AxiosResponse<IGetMovieVideoResponse>>(
        `${GET_MOVIE_BY_ID}/${id}/videos`,
        { params }
      )
    ).data;
  };
  return useQuery(`get-movie-video-by-id-${id}`, getMovieVideos);
}
