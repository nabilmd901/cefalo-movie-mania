import axios, { AxiosResponse } from "axios";
import { useQuery } from "react-query";
import { GET_MOVIES_BY_GENRE } from "..";
import { IGetMoviesByGenreResponse } from "../contracts";

export function useGetMovieByGenre(id?: string, sortBy?: string) {
  const getMoviesByGenre = async () => {
    let params: any = {
      with_genres: id,
    };
    if (sortBy?.length) params.sort_by = sortBy;
    return (
      await axios.get<null, AxiosResponse<IGetMoviesByGenreResponse>>(
        GET_MOVIES_BY_GENRE,
        { params }
      )
    ).data.results.slice(0, 10);
  };
  return useQuery(`get-movies-by-genre-${id}`, getMoviesByGenre);
}
