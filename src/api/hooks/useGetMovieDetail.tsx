import axios, { AxiosResponse } from "axios";
import { useQuery } from "react-query";
import { GET_MOVIE_BY_ID } from "..";
import { ApiKey } from "../../constants";
import { IGetMovieByIdResponse } from "../contracts";

export function useGetMovieDetail(id?: string) {
  const getMovieDetail = async () => {
    let params: any = {
      api_key: ApiKey,
    };
    return (
      await axios.get<null, AxiosResponse<IGetMovieByIdResponse>>(
        `${GET_MOVIE_BY_ID}/${id}`,
        { params }
      )
    ).data;
  };
  return useQuery(`get-movies-by-id-${id}`, getMovieDetail);
}
