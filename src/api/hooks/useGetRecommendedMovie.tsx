import axios, { AxiosResponse } from "axios";
import { useQuery } from "react-query";
import { GET_MOVIE_BY_ID } from "..";
import { ApiKey } from "../../constants";
import { IGetMoviesByGenreResponse } from "../contracts";

export function useGetRecommendedMovie(id?: string) {
  const getRecommendedMovie = async () => {
    let params: any = {
      api_key: ApiKey,
    };
    return (
      await axios.get<null, AxiosResponse<IGetMoviesByGenreResponse>>(
        `${GET_MOVIE_BY_ID}/${id}/recommendations`,
        { params }
      )
    ).data;
  };
  return useQuery(`get-reccomended-movies-by-id-${id}`, getRecommendedMovie);
}
