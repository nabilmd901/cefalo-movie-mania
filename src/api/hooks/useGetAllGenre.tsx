import axios, { AxiosResponse } from "axios";
import { useQuery } from "react-query";
import { GET_GENRE_LIST } from "..";
import { IGetGenreResponse } from "../contracts";

export function useGetAllGenre() {
  const getAllGenre = async () => {
    return (
      await axios.get<null, AxiosResponse<IGetGenreResponse>>(GET_GENRE_LIST)
    ).data.genres;
  };
  return useQuery("get-all-genre", getAllGenre);
}
