export interface IGetGenreResponse {
    genres: {
        id: number,
        name: string
    }[]
}

export interface IGetMoviesByGenreResponse {
    page: number,
    results: {
        id: number;
        poster_path: string;
        title: string,
        vote_average: number,
        popularity: number
    }[]
}

export interface IGetMovieByIdResponse {
    id: number;
    genres: {
        id: number;
        name: string;
    }[];
    imdb_id: string;
    overview: string;
    poster_path: string;
    release_date: string;
    title: string;
    vote_average: number;
}

export interface ICastDetail {
    id: string;
    gender: number;
    name: string;
    profile_path: string;
    character?: string;
    job?: string;
}
export interface IGetMovieCreditsByIdResponse {
    id: number;
    cast: ICastDetail[];
    crew: ICastDetail[];
}

export interface IGetMovieVideoResponse {
    results: {
        key: string,
        site: string,
        type: string,
    }[]
}

export interface IMovieLazyData {
    genre: string;
    id: number;
    movies: {
        id: number;
        poster_path: string;
        title: string;
        vote_average: number;
        popularity: number;
    }[];
}