import { IGetMovieVideoResponse } from "./api/contracts";
import { ImageBaseURL } from "./constants";
import { IMovieData } from "./context/MovieContext/contracts";

export const getMoviePoster = (url: string): string => {
    return `${ImageBaseURL}/${url}`;
}

export const getMovieFromLocalStorage = (key: string): IMovieData[] => {
    const movies = localStorage.getItem(key) || "[]";
    if (JSON.parse(movies).length) {
        return JSON.parse(movies) as IMovieData[];
    }
    return [];
}

export const addMovieToLocalStorage = (key: string, movieData: IMovieData[]) => {
    localStorage.setItem(key, JSON.stringify(movieData));
}

export const getOfficialTrailer = (videoData?: IGetMovieVideoResponse): string => {
    let result = "";
    const video = videoData?.results?.find(m => m.site === "YouTube" && m.type === "Trailer");
    if (video) result = video.key;
    return result;
}

