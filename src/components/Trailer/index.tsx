export interface ITrailerProps {
  videoId?: string;
}

function Trailer(props: ITrailerProps) {
  const { videoId } = props;
  return (
    <div className="w-[300px] h-[200px] md:w-[600px] md:h-[400px] lg:w-[800px] lg:h-[600px]">
      <iframe
        title="Trailer"
        className="w-[300px] h-[200px] md:w-[600px] md:h-[400px] lg:w-[800px] lg:h-[600px]"
        src={`https://www.youtube.com/embed/${videoId}?autoplay=1`}
        allow="autoplay; encrypted-media"
        allowFullScreen
      ></iframe>
    </div>
  );
}
export default Trailer;
