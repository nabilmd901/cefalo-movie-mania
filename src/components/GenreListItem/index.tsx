import { NavLink } from "react-router-dom";

export interface IGenreListItem {
  id: string;
  name: string;
}

function GenreListItem({ id, name }: IGenreListItem) {
  return (
    <NavLink to={`/genres/${id.toString()}`}>
      {({ isActive }) => (
        <div
          className={`m-1 py-1 px-2 rounded-2xl  border-2  ${
            isActive
              ? "border-link-active text-link-active"
              : "border-white text-white"
          }`}
        >
          {name}
        </div>
      )}
    </NavLink>
  );
}

export default GenreListItem;
