import { FadeLoader } from "react-spinners";

function Loader({ relative }: { relative?: boolean }) {
  return (
    <>
      {relative ? (
        <FadeLoader color="#ffffff" />
      ) : (
        <div className="fixed z-50 top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
          <FadeLoader color="#ffffff" />
        </div>
      )}
    </>
  );
}

export default Loader;
