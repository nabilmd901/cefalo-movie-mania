import { AiOutlineInfoCircle } from "react-icons/ai";

function NoData({ message }: { message: string }) {
  return (
    <div className="w-full text-center text-white flex flex-row items-center justify-center col-span-5">
      <AiOutlineInfoCircle className="text-white" />
      &nbsp;
      <div>{message}</div>
    </div>
  );
}

export default NoData;
