import { SiImdb } from "react-icons/si";
import { FaPlay } from "react-icons/fa";
import MovieCard from "../MovieCard";
import GenreListItem from "../GenreListItem";
import { IMDbBaseURL } from "../../constants";
import { useState } from "react";
import Trailer from "../Trailer";
import Modal from "../Modal";

export interface IMovieSummaryProps {
  id: number;
  name: string;
  image: string;
  rating: string;
  releaseDate: string;
  genres: {
    id: number;
    name: string;
  }[];
  overview: string;
  imdbLink: string;
  trailerId?: string;
}

function MovieSummary({
  id,
  name,
  image,
  rating,
  releaseDate,
  genres,
  overview,
  imdbLink,
  trailerId,
}: IMovieSummaryProps) {
  const [openModal, setOpenModal] = useState(false);

  return (
    <div>
      <Modal open={openModal} onClose={() => setOpenModal(false)}>
        <Trailer videoId={trailerId} />
      </Modal>
      <div className="bg-primary flex flex-col md:flex-row md:items-center items-start justify-center rounded-lg p-3">
        <div className="p-5 mx-auto">
          <MovieCard
            id={id}
            image={image}
            name={name}
            rating={rating}
            showName={false}
          />
        </div>
        <div className="flex flex-col w-full">
          <div className="px-5 w-full text-3xl text-white mb-5 flex flex-col items-center justify-center">
            <div className="w-full text-center md:text-left">
              {name} ({releaseDate})
            </div>
          </div>
          <div className="px-5 flex flex-row justify-start items-center flex-wrap">
            {genres.map((genre) => (
              <GenreListItem
                key={genre.id}
                id={genre.id?.toString()}
                name={genre.name}
              />
            ))}
          </div>
          <div className="px-5 w-full text-xl text-white text-justify italic">
            {overview}
          </div>
          <div className="mt-5 px-5 w-full flex items-center justify-start gap-5">
            <a
              target="__blank"
              href={`${IMDbBaseURL}/${imdbLink}`}
              className="inline-block text-white"
            >
              <SiImdb className="text-7xl" />
            </a>
            <button
              className="text-white flex flex-row items-center border-2 px-3 py-3 rounded-md"
              onClick={() => setOpenModal(true)}
            >
              <FaPlay className="text-2xl" />{" "}
              <span className="ml-2">Play Trailer</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MovieSummary;
