import { AiOutlineInfoCircle } from "react-icons/ai";
import Pagination from "../Pagination";
import { ICastDetail } from "../../api/contracts";
import { usePagination } from "../../hooks/usePagination";
import Avatar from "../Avatar";

function CastList({ data }: { data: ICastDetail[] }) {
  const [paginatedData, setPaginatedPage] = usePagination({
    data: data || [],
    limit: 12,
  });

  return (
    <div>
      <div className="bg-primary p-3 rounded-lg">
        <div>
          {!paginatedData?.data?.length && (
            <div className="text-center text-white flex flex-row items-center justify-center">
              <AiOutlineInfoCircle className="text-white" />
              &nbsp;
              <div>No data available</div>
            </div>
          )}
        </div>
        <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 ">
          {paginatedData?.data?.map((c) => (
            <Avatar
              key={Math.random()}
              name={c.name}
              character={c.character}
              image={c.profile_path}
              gender={c.gender}
              id={c.id}
              job={c.job}
            />
          ))}
        </div>
      </div>
      {paginatedData?.data?.length ? (
        <Pagination
          totalPage={paginatedData.totalPage}
          pageChangeHandler={setPaginatedPage}
          currentPage={paginatedData.currentPage}
        />
      ) : null}
    </div>
  );
}

export default CastList;
