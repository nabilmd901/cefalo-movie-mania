import { toast } from "react-toastify";
import { SwiperSlide } from "swiper/react";
import { useGetRecommendedMovie } from "../../api/hooks/useGetRecommendedMovie";
import Loader from "../Loader";
import MovieCard from "../MovieCard";
import NoData from "../NoData";
import Slider from "../Slider";

function RelatedMovieList({ movieId }: { movieId?: string }) {
  const { isLoading, isError, data } = useGetRecommendedMovie(movieId);

  return (
    <div>
      {isLoading && <Loader />}
      {!data?.results?.length ? <NoData message="No related movies" /> : null}
      <Slider>
        {data?.results.map((d) => (
          <SwiperSlide key={Math.random()}>
            <MovieCard
              id={d.id}
              image={d.poster_path}
              name={d.title}
              rating={d.vote_average?.toString()}
            />
          </SwiperSlide>
        ))}
      </Slider>
      {isError &&
        toast.error("Failed to load Recommendations", {
          toastId: "fetch-by-genre-error",
          position: "bottom-right",
        })}
    </div>
  );
}

export default RelatedMovieList;
