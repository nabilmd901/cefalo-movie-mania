import { Outlet } from "react-router-dom";
import Navbar from "../Navbar";

function Layout() {
  return (
    <div>
      <Navbar />
      <div className="pt-24 pb-10 container mx-auto px-5 lg:px-0">
        <Outlet />
      </div>
    </div>
  );
}

export default Layout;
