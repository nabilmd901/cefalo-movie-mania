import { Dispatch, SetStateAction } from "react";

function Pagination({
  currentPage,
  totalPage,
  pageChangeHandler,
}: {
  currentPage: number;
  totalPage: number;
  pageChangeHandler: Dispatch<SetStateAction<number>>;
}) {
  const renderPagination = () => {
    const buttons = [];
    for (let i = 0; i < totalPage; i++) {
      buttons.push(
        <button
          className={`w-8 text-white hover:bg-primary rounded-lg ${
            currentPage === i + 1 ? "bg-primary" : "bg-slate-600"
          }`}
          key={Math.random()}
          onClick={() => pageChangeHandler(i + 1)}
        >
          {i + 1}
        </button>
      );
    }
    return buttons;
  };

  return (
    <div className="mt-5 w-full flex flex-row flex-wrap justify-center items-center gap-4">
      {renderPagination()}
    </div>
  );
}

export default Pagination;
