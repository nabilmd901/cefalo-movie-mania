import React from "react";
import { Swiper } from "swiper/react";
import "swiper/css";

function Slider({ children }: React.PropsWithChildren) {
  return (
    <Swiper
      spaceBetween={5}
      slidesPerView={1}
      breakpoints={{
        426: {
          slidesPerView: 3,
        },
        1026: {
          slidesPerView: 5,
        },
      }}
    >
      {children}
    </Swiper>
  );
}

export default Slider;
