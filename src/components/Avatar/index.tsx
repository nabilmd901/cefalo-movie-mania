import { getMoviePoster } from "../../utils";
import MaleImage from "../../assets/male.jpg";
import FemaleImage from "../../assets/female.png";

export interface IAvatarProps {
  image: string;
  name: string;
  character?: string | null;
  job?: string | null;
  id: string;
  gender: number;
}

function Avatar({ image, name, character, job, gender }: IAvatarProps) {
  let pic = getMoviePoster(image);

  if (!image) {
    pic = gender === 1 ? FemaleImage : MaleImage;
  }
  return (
    <div className="p-2 flex flex-row justify-start items-center">
      <div className="w-24 h-24 rounded-[50%] overflow-hidden">
        <img className="object-contain" src={pic} alt={name} />
      </div>
      <div className="ml-5 text-white">
        <div className="font-semibold text-xl">{name}</div>
        {character ? (
          <div className="font-thin text-sm italic">{character}</div>
        ) : (
          <div className="font-thin text-sm italic">{job}</div>
        )}
      </div>
    </div>
  );
}

export default Avatar;
