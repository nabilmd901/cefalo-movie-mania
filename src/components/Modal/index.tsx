import { PropsWithChildren } from "react";
import ReactDOM from "react-dom";

export interface IModalProps {
  open: boolean;
  onClose: () => void;
}

function Modal({ open, children, onClose }: PropsWithChildren<IModalProps>) {
  const renderModal = () => {
    return (
      <div className="fixed top-0 left-0 w-screen h-screen" onClick={onClose}>
        <div className=" bg-primary overflow-hidden absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
          {children}
        </div>
      </div>
    );
  };
  return ReactDOM.createPortal(
    <>{open ? renderModal() : null}</>,
    document.getElementById("modal")!
  );
}

export default Modal;
