import { toast } from "react-toastify";
import { useGetAllGenre } from "../../api/hooks/useGetAllGenre";
import GenreListItem from "../GenreListItem";
import Loader from "../Loader";

function GenreList() {
  const { isLoading, isError, data } = useGetAllGenre();

  return (
    <div>
      {isLoading && <Loader />}
      <div className="w-full flex flex-row items-center justify-start flex-wrap">
        {data?.map((d) => (
          <GenreListItem key={d.id} id={d.id.toString()} name={d.name} />
        ))}
      </div>
      {isError &&
        toast.error("Failed to load data", {
          toastId: "fetch-genre-error",
          position: "bottom-right",
        })}
    </div>
  );
}

export default GenreList;
