import { useEffect } from "react";
import { useLocation } from "react-router-dom";

const ScrollToTop = ({ children }: { children: any }) => {
  const location = useLocation();

  useEffect(() => {
    window.scroll({
      top: 0,
    });
  }, [location]);

  return children;
};

export default ScrollToTop;
