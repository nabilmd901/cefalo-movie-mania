import { Link, NavLink } from "react-router-dom";

function Navbar() {
  return (
    <div className="fixed w-screen z-50 bg-primary">
      <div className="container h-20 w-screen mx-auto flex flex-row items-center justify-between text-white px-5 lg:px-0">
        <Link to={"/"} className="text-sm md:text-xl font-bold">
          Movie Mania
        </Link>
        <div className="flex flex-row gap-5 text-xs md:text-lg items-center justify-between">
          <NavLink to="genres">
            {({ isActive }) => (
              <div
                className={`${
                  isActive ? "text-link-active font-semibold" : "text-white"
                }`}
              >
                Genres
              </div>
            )}
          </NavLink>
          <NavLink to="watchlist">
            {({ isActive }) => (
              <div
                className={`${
                  isActive ? "text-link-active font-semibold" : "text-white"
                }`}
              >
                Watchlist
              </div>
            )}
          </NavLink>
          <NavLink to="recently-visited">
            {({ isActive }) => (
              <div
                className={`${
                  isActive ? "text-link-active font-semibold" : "text-white"
                }`}
              >
                Recently Visited
              </div>
            )}
          </NavLink>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
