import { FaStar } from "react-icons/fa";
import { BsGraphUp } from "react-icons/bs";
import { AiOutlinePlus } from "react-icons/ai";
import { AiOutlineCheck } from "react-icons/ai";
import { useContext } from "react";
import { MovieContext } from "../../context/MovieContext";
import {
  addHistory,
  addToWatchList,
  removeFromWatchList,
} from "../../context/MovieContext/actions";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import { getMoviePoster } from "../../utils";
import DefaultMovieImage from "../../assets/movieDefault.png";

export interface IMovieCardProps {
  id: number;
  image: string;
  rating: string;
  name: string;
  popularity?: number;
  showName?: boolean;
}

function MovieCard({
  id,
  image,
  rating,
  name,
  popularity,
  showName = true,
}: IMovieCardProps) {
  const { watchList, dispatch } = useContext(MovieContext);
  const movieImage = image ? getMoviePoster(image) : DefaultMovieImage;

  const isAddedToWatchList = !!watchList.find((wl) => wl.id === id);

  const addWatchList = () => {
    dispatch(
      addToWatchList({
        id,
        title: name,
        poster_path: image,
        vote_average: parseInt(rating),
        addedAt: new Date().getTime(),
      })
    );
    toast.success(`${name} added to watchlist`, {
      position: "bottom-right",
    });
  };

  const addToHistory = () => {
    dispatch(
      addHistory({
        id,
        title: name,
        poster_path: image,
        vote_average: parseInt(rating),
        addedAt: new Date().getTime(),
      })
    );
  };

  const removeWatchList = (id: number) => {
    dispatch(removeFromWatchList({ id }));
    toast.error(`${name} removed from watchlist`, {
      position: "bottom-right",
    });
  };

  return (
    <div className="relative overflow-hidden h-auto mx-auto hover:cursor-pointer">
      <img
        alt={name}
        className="peer object-contain mx-auto w-96 h-auto"
        src={movieImage}
      />

      {showName ? (
        <Link
          to={`/movies/${id}`}
          className="peer peer-hover:block hover:block absolute hidden top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2"
          onClick={addToHistory}
        >
          <div className=" bg-primary text-white opacity-80 px-3 py-2 text-sm">
            View Details
          </div>
        </Link>
      ) : null}

      <div
        className="absolute top-1 right-1 bg-slate-900 opacity-80 px-2 py-1 rounded text-sm flex flex-row items-center mb-2 text-white"
        data-testid="watchlisthandler"
        onClick={isAddedToWatchList ? () => removeWatchList(id) : addWatchList}
      >
        {isAddedToWatchList ? (
          <AiOutlineCheck className="cursor-pointer w-6 h-6" />
        ) : (
          <AiOutlinePlus className="cursor-pointer w-6 h-6" />
        )}
      </div>

      <div
        data-testid="rating"
        className="absolute top-1 left-1 bg-slate-900 opacity-80 px-2 py-1 rounded text-yellow-500 flex flex-row items-center"
      >
        <FaStar /> &nbsp; {rating} / 10
      </div>

      <div className="absolute bottom-1 left-1 right-1">
        {popularity && (
          <div className="text-white w-max bg-slate-900 opacity-80  px-2 py-1 rounded text-sm flex flex-row items-center mb-2">
            <BsGraphUp /> &nbsp; {popularity}
          </div>
        )}
      </div>

      <div
        data-testid="movie-name"
        className="absolute -bottom-36 peer-hover:bottom-1 hover:bottom-1 duration-200 left-1 right-1"
      >
        {name && showName && (
          <div className="text-white bg-slate-900 opacity-80 px-2 py-1 rounded">
            {name}
          </div>
        )}
      </div>
    </div>
  );
}

export default MovieCard;
