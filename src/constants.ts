export const BaseURL = process.env.REACT_APP_BASEURL;
export const ApiKey = process.env.REACT_APP_API_KEY;
export const ImageBaseURL = process.env.REACT_APP_RESOURCE_BASEURL;
export const IMDbBaseURL = process.env.REACT_APP_IMDB_BASEURL;


export const LAZYLOAD_INITIAL_LIMIT = 2;