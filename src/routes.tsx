import React from "react";
import { Navigate, RouteObject } from "react-router-dom";

const Layout = React.lazy(() => import("./components/Layout"));

const Movies = React.lazy(() => import("./pages/Movies"));
const MovieDetail = React.lazy(() => import("./pages/MovieDetail"));
const Genres = React.lazy(() => import("./pages/Genres"));
const MovieListGenre = React.lazy(
  () => import("./pages/Genres/MovieListGenre")
);
const WatchList = React.lazy(() => import("./pages/WatchList"));
const RecentlyVisited = React.lazy(() => import("./pages/RecentlyVisited"));
const Page404 = React.lazy(() => import("./pages/Page404"));

export const ROUTES: RouteObject[] = [
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "",
        element: <Navigate to="movies" />,
      },
      {
        path: "movies",
        element: <Movies />,
      },
      {
        path: "movies/:id",
        element: <MovieDetail />,
      },
      {
        path: "genres",
        element: <Genres />,
        children: [
          {
            path: ":id",
            element: <MovieListGenre />,
          },
        ],
      },
      {
        path: "watchlist",
        element: <WatchList />,
      },
      {
        path: "recently-visited",
        element: <RecentlyVisited />,
      },
      {
        path: "*",
        element: <Page404 />,
      },
    ],
  },
];
