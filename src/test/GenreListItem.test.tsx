import { BrowserRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import GenreListItem from "../components/GenreListItem";

const MockGenreData = {
  id: 1,
  name: "action",
};

const MockGenreList = () => {
  return (
    <BrowserRouter>
      <GenreListItem
        id={MockGenreData.id.toString()}
        name={MockGenreData.name}
      />
    </BrowserRouter>
  );
};

describe("<GenreListItem/>", () => {
  beforeEach(() => {
    render(<MockGenreList />);
  });

  it("has correct genre link", () => {
    const linkElement = screen.getByText(new RegExp(MockGenreData.name, "i"));
    expect(linkElement.closest("a")).toHaveAttribute(
      "href",
      `/genres/${MockGenreData.id}`
    );
  });

  it("has correct genre name", () => {
    const genreNameElement = screen.getByText(
      new RegExp(MockGenreData.name, "i")
    );
    expect(genreNameElement).toBeInTheDocument();
  });
});
