import { BrowserRouter } from "react-router-dom";
import { render, screen } from "@testing-library/react";
import MovieCard from "../components/MovieCard";
import { getMoviePoster } from "../utils";

const MockMovieData = {
  id: 766507,
  title: "Prey",
  poster_path: "/ujr5pztc1oitbe7ViMUOilFaJ7s.jpg",
  vote_average: 8,
  popularity: 1000,
};

const MockMovieCard = () => {
  return (
    <BrowserRouter>
      <MovieCard
        id={MockMovieData.id}
        image={MockMovieData.poster_path}
        rating={MockMovieData.vote_average.toString()}
        name={MockMovieData.title}
        popularity={MockMovieData.popularity}
      />
    </BrowserRouter>
  );
};

describe("<MovieCard/>", () => {
  beforeEach(() => {
    render(<MockMovieCard />);
  });
  // Testing Rating Element
  describe("Rating", () => {
    let ratingElement: HTMLElement;
    beforeEach(() => {
      ratingElement = screen.getByTestId("rating");
    });
    it("has rating element", () => {
      expect(ratingElement).toBeInTheDocument();
    });
    it("has correct rating value", () => {
      const regex = new RegExp(MockMovieData.vote_average.toString(), "i");
      expect(ratingElement.innerHTML).toMatch(regex);
    });
  });
  // Testing watchlist handler
  describe("Watchlist handler", () => {
    let watchlistElement: HTMLElement;
    beforeEach(() => {
      watchlistElement = screen.getByTestId("watchlisthandler");
    });
    it("has watchlist icon", () => {
      expect(watchlistElement).toBeInTheDocument();
    });
  });
  // Testing watchlist handler
  describe("View Details", () => {
    let viewDetailsElement: HTMLElement;
    beforeEach(() => {
      viewDetailsElement = screen.getByText(/View Details/i);
    });
    it("has View Details element", () => {
      expect(viewDetailsElement).toBeInTheDocument();
    });
  });
  // Testing Movie name
  describe("Movie Name", () => {
    it("has movie name", () => {
      const regex = new RegExp(MockMovieData.title, "i");
      const movieElement = screen.getByText(regex);
      expect(movieElement).toBeInTheDocument();
    });
  });
  // Testing Movie popularity
  describe("Movie Popularity", () => {
    it("has papularity", () => {
      const regex = new RegExp(MockMovieData.popularity.toString(), "i");
      const popularityElement = screen.getByText(regex);
      expect(popularityElement).toBeInTheDocument();
    });
  });
  // Testing movie link
  describe("Movie Link", () => {
    it("has proper movie link", () => {
      const linkElement = screen.getByText(/View Details/i);
      expect(linkElement.closest("a")).toHaveAttribute(
        "href",
        `/movies/${MockMovieData.id}`
      );
    });
  });
  // Testing movie poster
  describe("Movie Poster", () => {
    it("has correct image src", () => {
      const imageElement = screen.getByRole("img");
      expect(imageElement).toHaveAttribute(
        "src",
        getMoviePoster(MockMovieData.poster_path)
      );
    });
    it("has correct image alt", () => {
      const imageElement = screen.getByRole("img");
      expect(imageElement).toHaveAttribute("alt", MockMovieData.title);
    });
  });
});
