import { render, screen, waitFor } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Avatar from "../components/Avatar";
import { getMoviePoster } from "../utils";

const MockAvatarData = {
  image: "/jpurJ9jAcLCYjgHHfYF32m3zJYm.jpg",
  name: "Chris Hemsworth",
  character: "Thor Odinson",
  job: "Thor Odinson",
  id: 1234,
  gender: 2,
};

const MockAvatar = () => {
  return (
    <BrowserRouter>
      <Avatar
        image={MockAvatarData.image}
        name={MockAvatarData.name}
        id={MockAvatarData.id.toString()}
        gender={MockAvatarData.gender}
        character={MockAvatarData.character}
      />
    </BrowserRouter>
  );
};

describe("<Avatar/>", () => {
  beforeEach(() => {
    render(<MockAvatar />);
  });
  describe("Image", () => {
    let imageElement: HTMLElement;
    beforeEach(() => {
      imageElement = screen.getByRole("img");
    });
    it("has correct image src", () => {
      expect(imageElement).toHaveAttribute(
        "src",
        getMoviePoster(MockAvatarData.image)
      );
    });
    it("has correct image alt", () => {
      expect(imageElement).toHaveAttribute("alt", MockAvatarData.name);
    });
  });

  it("has correct name", () => {
    const nameElement = screen.getByText(new RegExp(MockAvatarData.name, "i"));
    expect(nameElement).toBeInTheDocument();
  });

  it("has correct character", () => {
    const characterElement = screen.getByText(
      new RegExp(MockAvatarData.character, "i")
    );
    expect(characterElement).toBeInTheDocument();
  });

  it("has correct job", () => {
    const jobElement = screen.getByText(new RegExp(MockAvatarData.job, "i"));
    expect(jobElement).toBeInTheDocument();
  });
});
