export interface IMovieData {
    id: number;
    poster_path: string;
    title: string,
    vote_average: number,
    addedAt: number;
}
