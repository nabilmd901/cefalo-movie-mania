import { IMovieContextData } from ".";
import { addMovieToLocalStorage, getMovieFromLocalStorage } from "../../utils";
import { HISTORY_ADD, HISTORY_CLEAR, HISTORY_FETCH, MovieActions, WATCHLIST_ADD, WATCHLIST_FETCH, WATCHLIST_REMOVE } from "./actionTypes";
import { IMovieData } from "./contracts";

function sortMoviesByDate(movieData: IMovieData[]): IMovieData[] {
    return [...movieData].sort((a, b) => b.addedAt - a.addedAt);
}

function removeMovie(movieData: IMovieData[], id: number): IMovieData[] {
    const filteredData = [...movieData].filter(movie => movie.id !== id);
    return sortMoviesByDate(filteredData);
}

function fetchMovie(state: IMovieContextData, localStorageKey: "history" | "watchList"): IMovieContextData {
    const movies = getMovieFromLocalStorage(localStorageKey);
    const sorted = sortMoviesByDate(movies);
    addMovieToLocalStorage(localStorageKey, sorted);
    return { ...state, [localStorageKey]: [...sorted] };
}

function addMovie(state: IMovieContextData, movie: IMovieData, localStorageKey: "history" | "watchList"): IMovieContextData {
    let movieData = [...state[localStorageKey]];
    const foundMovieIndex = movieData.findIndex(m => m.id === movie.id);
    if (foundMovieIndex < 0) {
        movieData.push(movie);
        const movieAftedAdd = sortMoviesByDate(movieData);
        addMovieToLocalStorage(localStorageKey, movieAftedAdd);
        return { ...state, [localStorageKey]: movieAftedAdd };
    } else if (localStorageKey === "history" && foundMovieIndex >= 0) {
        movieData[foundMovieIndex].addedAt = (new Date()).getTime();
        const movieUpdate = sortMoviesByDate(movieData);
        addMovieToLocalStorage(localStorageKey, movieUpdate);
        return { ...state, [localStorageKey]: movieUpdate };
    }
    return { ...state };
}

export default function movieReducer(state: IMovieContextData, action: MovieActions) {
    switch (action.type) {
        case WATCHLIST_FETCH:
            return fetchMovie({ ...state }, "watchList");
        case WATCHLIST_ADD:
            return addMovie({ ...state }, action.payload, "watchList");
        case WATCHLIST_REMOVE:
            const movieAfterRemove = removeMovie([...state.watchList], action.payload.id);
            addMovieToLocalStorage("watchList", movieAfterRemove);
            return { ...state, watchList: movieAfterRemove };
        case HISTORY_FETCH:
            return fetchMovie({ ...state }, "history");
        case HISTORY_ADD:
            return addMovie({ ...state }, action.payload, "history");
        case HISTORY_CLEAR:
            addMovieToLocalStorage("history", []);
            return { ...state, history: [] };
        default:
            return state;
    }
}