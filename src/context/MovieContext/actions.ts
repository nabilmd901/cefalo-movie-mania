import { HISTORY_ADD, HISTORY_CLEAR, HISTORY_FETCH, WATCHLIST_ADD, WATCHLIST_FETCH, WATCHLIST_REMOVE } from "./actionTypes";
import { IMovieData } from "./contracts";

export const fetchWatchList = (): { type: typeof WATCHLIST_FETCH } => {
    return {
        type: WATCHLIST_FETCH,
    };
};

export const addToWatchList = (data: IMovieData): { type: typeof WATCHLIST_ADD, payload: IMovieData } => {
    return {
        type: WATCHLIST_ADD,
        payload: data,
    };
};

export const removeFromWatchList = (data: { id: number }): { type: typeof WATCHLIST_REMOVE, payload: { id: number } } => {
    return {
        type: WATCHLIST_REMOVE,
        payload: data,
    };
};

export const fetchHsitory = (): { type: typeof HISTORY_FETCH } => {
    return {
        type: HISTORY_FETCH,
    };
};

export const addHistory = (data: IMovieData): { type: typeof HISTORY_ADD, payload: IMovieData } => {
    return {
        type: HISTORY_ADD,
        payload: data,
    };
};

export const clearHistory = (): { type: typeof HISTORY_CLEAR } => {
    return {
        type: HISTORY_CLEAR,
    };
}