import { IMovieData } from "./contracts";

export const WATCHLIST_FETCH = "watchlist/fetch";
export interface WatchListFetchAction {
    type: typeof WATCHLIST_FETCH;
}

export const WATCHLIST_ADD = "watchlist/add";
export interface WatchListAddAction {
    type: typeof WATCHLIST_ADD;
    payload: IMovieData;
}

export const WATCHLIST_REMOVE = "watchlist/remove";
export interface WatchListRemoveAction {
    type: typeof WATCHLIST_REMOVE;
    payload: { id: number };
}

export const HISTORY_FETCH = "history/fetch";
export interface HistoryFetchAction {
    type: typeof HISTORY_FETCH;
}

export const HISTORY_ADD = "history/add";
export interface HistoryAddAction {
    type: typeof HISTORY_ADD;
    payload: IMovieData;
}

export const HISTORY_CLEAR = "history/clear";
export interface HistoryClearAction {
    type: typeof HISTORY_CLEAR;
}


export type MovieActions = WatchListFetchAction | WatchListAddAction | WatchListRemoveAction | HistoryFetchAction | HistoryAddAction | HistoryClearAction;