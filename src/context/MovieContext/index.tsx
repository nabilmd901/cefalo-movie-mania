import { createContext, PropsWithChildren, useEffect, useReducer } from "react";
import { fetchHsitory, fetchWatchList } from "./actions";
import { IMovieData } from "./contracts";
import movieReducer from "./movieReducer";

export interface IMovieContextData {
  watchList: IMovieData[];
  history: IMovieData[];
  dispatch: React.Dispatch<any>;
}

const initialState: IMovieContextData = {
  watchList: [],
  history: [],
  dispatch: () => null,
};

export const MovieContext = createContext<IMovieContextData>(initialState);

export const MovieContextProvider = (props: PropsWithChildren) => {
  const [state, dispatch] = useReducer(movieReducer, initialState);

  const fetchMovieWatchList = () => {
    const action = fetchWatchList();
    dispatch(action);
  };

  const fetchMovieHistory = () => {
    const action = fetchHsitory();
    dispatch(action);
  };

  useEffect(() => {
    fetchMovieWatchList();
    fetchMovieHistory();
  }, []);

  return (
    <MovieContext.Provider
      value={{
        watchList: state.watchList,
        history: state.history,
        dispatch,
      }}
    >
      {props.children}
    </MovieContext.Provider>
  );
};
