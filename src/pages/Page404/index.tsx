import { FaRegSadTear } from "react-icons/fa";

function Page404() {
  return (
    <div className="text-white absolute left-1/2 top-[40%] -translate-x-1/2 -translate-y-1/1">
      <div className="flex flex-col items-center justify-center">
        <FaRegSadTear className="text-[10rem]" />
        <div className="mt-2 text-4xl">404 - Page not found!</div>
      </div>
    </div>
  );
}

export default Page404;
