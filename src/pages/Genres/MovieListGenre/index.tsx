import { useParams } from "react-router-dom";
import { useGetMovieByGenre } from "../../../api/hooks/useGetMovieByGenre";
import MovieCard from "../../../components/MovieCard";
import Loader from "../../../components/Loader";

function MovieListGenre() {
  const params = useParams<{ id: string }>();
  const { isLoading, data } = useGetMovieByGenre(params?.id, "popularity.desc");

  return (
    <div>
      {isLoading && <Loader />}

      <div key={Math.random()} className="mt-10">
        <h1 className="text-white font-bold text-2xl mb-5">
          Sorted By Popularity
        </h1>

        <div className="grid grid-cols-1 md:grid-cols-5 gap-2 mx-auto">
          {data?.map((d) => (
            <div key={Math.random()} className="mx-auto">
              <MovieCard
                id={d.id}
                image={d.poster_path}
                name={d.title}
                rating={d.vote_average?.toString()}
                popularity={d.popularity}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default MovieListGenre;
