import { Outlet } from "react-router-dom";
import GenreList from "../../components/GenreList";

function Genres() {
  return (
    <div>
      <GenreList />
      <Outlet />
    </div>
  );
}

export default Genres;
