import { useContext } from "react";
import MovieCard from "../../components/MovieCard";
import NoData from "../../components/NoData";
import Pagination from "../../components/Pagination";
import { MovieContext } from "../../context/MovieContext";
import { clearHistory } from "../../context/MovieContext/actions";
import { usePagination } from "../../hooks/usePagination";

function RecentlyVisited() {
  const { history, dispatch } = useContext(MovieContext);
  const [paginatedData, setPaginatedPage] = usePagination({
    data: history || [],
    limit: 10,
  });

  const onClearHistory = () => {
    dispatch(clearHistory());
  };

  return (
    <div className="mx-auto">
      <div className="w-full flex flex-row-reverse">
        {paginatedData?.data?.length ? (
          <button
            className="bg-primary text-white px-3 py-2 cursor-pointer rounded-lg mb-3"
            onClick={onClearHistory}
          >
            Clear History
          </button>
        ) : null}
      </div>
      <div className="grid grid-cols-1 md:grid-cols-5 gap-2">
        {!paginatedData?.data?.length ? (
          <NoData message="No history data found" />
        ) : null}
        {paginatedData?.data?.map((movie) => (
          <div
            key={Math.random()}
            className="flex flex-col justify-center items-center mx-auto"
          >
            <MovieCard
              id={movie.id}
              image={movie.poster_path}
              name={movie.title}
              rating={movie.vote_average.toString()}
            />
            <h1 className="mt-2 text-sm text-white">
              <span className="font-semibold">Last Visited On:</span>&nbsp;
              {new Date(movie.addedAt).toLocaleString()}
            </h1>
          </div>
        ))}
      </div>
      {paginatedData?.data?.length ? (
        <Pagination
          totalPage={paginatedData.totalPage}
          pageChangeHandler={setPaginatedPage}
          currentPage={paginatedData.currentPage}
        />
      ) : null}
    </div>
  );
}

export default RecentlyVisited;
