import { useContext } from "react";
import MovieCard from "../../components/MovieCard";
import NoData from "../../components/NoData";
import Pagination from "../../components/Pagination";
import { MovieContext } from "../../context/MovieContext";
import { usePagination } from "../../hooks/usePagination";

function WatchList() {
  const { watchList } = useContext(MovieContext);
  const [paginatedData, setPaginatedPage] = usePagination({
    data: watchList || [],
    limit: 10,
  });

  return (
    <div>
      <div className="grid grid-cols-1 md:grid-cols-5 gap-2">
        {!paginatedData?.data?.length ? (
          <NoData message="No movie in watchlist" />
        ) : null}
        {paginatedData?.data?.map((movie) => (
          <div
            key={Math.random()}
            className="flex flex-col justify-center items-center mx-auto"
          >
            <MovieCard
              id={movie.id}
              image={movie.poster_path}
              name={movie.title}
              rating={movie.vote_average.toString()}
            />
            <h1 className="mt-2 text-sm text-white">
              <span className="font-semibold">Date Added:</span>&nbsp;
              {new Date(movie.addedAt).toLocaleString()}
            </h1>
          </div>
        ))}
      </div>
      {paginatedData?.data?.length ? (
        <Pagination
          totalPage={paginatedData.totalPage}
          pageChangeHandler={setPaginatedPage}
          currentPage={paginatedData.currentPage}
        />
      ) : null}
    </div>
  );
}

export default WatchList;
