import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { useGetCastAndCrew } from "../../api/hooks/useGetCastAndCrew";
import { useGetMovieDetail } from "../../api/hooks/useGetMovieDetail";
import { useGetMovieVideos } from "../../api/hooks/useGetMovieVideos";
import CastList from "../../components/CastList";
import Loader from "../../components/Loader";
import MovieSummary from "../../components/MovieSummary";
import RelatedMovieList from "../../components/RelatedMovieList";
import { getOfficialTrailer } from "../../utils";

function MovieDetail() {
  const params = useParams<{ id: string }>();

  const {
    isError: movieError,
    isLoading: movieLoading,
    data: movieData,
  } = useGetMovieDetail(params.id);

  const {
    isError: creditsError,
    isLoading: creditsLoading,
    data: creditsData,
  } = useGetCastAndCrew(params.id);

  const { data: videoData } = useGetMovieVideos(params.id);

  return (
    <div>
      {/* Movie Summary Start*/}
      {movieLoading && <Loader />}
      {movieError &&
        toast.error("Failed to movie data", {
          toastId: "fetch-by-movie-id-error",
          position: "bottom-right",
        })}
      {movieData && (
        <MovieSummary
          id={movieData?.id}
          image={movieData?.poster_path}
          genres={movieData?.genres}
          imdbLink={movieData?.imdb_id}
          name={movieData?.title}
          overview={movieData.overview}
          rating={movieData?.vote_average.toString()}
          releaseDate={movieData.release_date}
          trailerId={getOfficialTrailer(videoData)}
        />
      )}
      {/* Movie Summary End*/}

      {/* Cast Detail Start */}
      {creditsError &&
        toast.error("Failed to cast data", {
          toastId: "fetch-cast-by-movie-id-error",
          position: "bottom-right",
        })}
      {creditsLoading && <Loader />}
      <div className="text-white font-bold text-4xl mt-5 mb-5">Cast</div>
      <CastList data={creditsData?.cast || []} />
      <div className="text-white font-bold text-4xl mt-5 mb-5">Crew</div>
      <CastList data={creditsData?.crew || []} />
      {/* Cast Detail End */}

      {/* Related movies Start */}
      <div className="text-white font-bold text-4xl mt-5 mb-5">
        Related movies
      </div>
      <RelatedMovieList movieId={params.id?.toString()} />
      {/* Related movies End */}
    </div>
  );
}

export default MovieDetail;
