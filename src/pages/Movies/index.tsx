import { useState } from "react";
import { SwiperSlide } from "swiper/react";
import { Link } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroller";
import axios, { AxiosResponse } from "axios";

import { LAZYLOAD_INITIAL_LIMIT } from "../../constants";
import Loader from "../../components/Loader";
import MovieCard from "../../components/MovieCard";
import Slider from "../../components/Slider";
import { useGetAllGenre } from "../../api/hooks/useGetAllGenre";
import { GET_MOVIES_BY_GENRE } from "../../api";
import { IGetMoviesByGenreResponse, IMovieLazyData } from "../../api/contracts";

function Movies() {
  const { data: genreData } = useGetAllGenre();
  const [movieData, setMovieData] = useState<IMovieLazyData[]>([]);
  const [start, setStart] = useState(0);
  const [hasMore, setHasMore] = useState(true);
  const [loading, setLoading] = useState(false);

  const getAllMovieByGenre = async () => {
    if (genreData && hasMore) {
      setLoading(true);
      const startIndex = start;
      const endIndex = start + LAZYLOAD_INITIAL_LIMIT;
      const genresArr = [...genreData].slice(startIndex, endIndex);

      const movies: IMovieLazyData[] = [];
      for await (let genre of genresArr) {
        const data = (
          await axios.get<null, AxiosResponse<IGetMoviesByGenreResponse>>(
            GET_MOVIES_BY_GENRE,
            {
              params: { with_genres: genre.id },
            }
          )
        ).data.results.slice(0, 5);
        movies.push({ genre: genre.name, id: genre.id, movies: data });
      }
      const newMovieData = [...movieData, ...movies];

      setMovieData(newMovieData);
      setStart(endIndex);
      if (newMovieData.length >= genreData.length) setHasMore(false);
      setLoading(false);
    }
  };

  return (
    <>
      {loading ? <Loader /> : null}
      <div>
        <InfiniteScroll loadMore={getAllMovieByGenre} hasMore={hasMore}>
          {movieData?.map((d) => (
            <div key={Math.random()}>
              <div className="my-5 flex flex-row items-baseline">
                <div className="text-white font-bold text-xl md:text-4xl">
                  {d.genre}
                </div>
                <Link
                  className="ml-5 block text-md md:text-xl hover:text-blue-500 text-white"
                  to={`/genres/${d.id.toString()}`}
                >
                  See more
                </Link>
              </div>

              <Slider>
                {d.movies.map((movie) => (
                  <SwiperSlide key={movie.id}>
                    <MovieCard
                      id={movie.id}
                      image={movie.poster_path}
                      name={movie.title}
                      rating={movie.vote_average?.toString()}
                    />
                  </SwiperSlide>
                ))}
              </Slider>
            </div>
          ))}
        </InfiniteScroll>
      </div>
    </>
  );
}

export default Movies;
