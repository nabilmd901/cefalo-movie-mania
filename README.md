# Cefalo - Movie Mania

A movie catalog website based on the data provided from [The Movie Database (TMDb)](https://www.themoviedb.org/).

## Installation Steps

***

### Clone the repository

```console
~$ git clone https://github.com/nabil404/cefalo-movie-mania.git
```

### Install dependencies

In the project directory `cefalo-movie-mania`, run:

```console
~/cefalo-movie-mania$ yarn
```

** If you do not have yarn installed, run the command to install yarn:

```console
~/$ npm i -g yarn
```

### Run the project

```console
~/cefalo-movie-mania$ yarn start
```

It will the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Other Scripts

***

### Run Tests

```console
~/cefalo-movie-mania$ yarn test
```

\
Launches the test runner in the interactive watch mode.

### Build the project

```console
~/cefalo-movie-mania$ yarn build
```

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
